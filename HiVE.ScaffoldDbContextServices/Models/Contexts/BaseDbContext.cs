﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace HiVE.ScaffoldDbContextServices.Models.Contexts
{
    public partial class BaseDbContext : DbContext
    {
        public BaseDbContext()
        {
        }

        public BaseDbContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public BaseDbContext(DbContextOptions<BaseDbContext> options, IConfiguration configuration)
            : base(options)
        {
            Configuration = configuration;
        }

        #region Common

        public IConfiguration Configuration { get; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(
                    $"server={Configuration["ConnectionString:Server"]};" +
                    $"port={Configuration["ConnectionString:Port"]};" +
                    $"user={Configuration["ConnectionString:User"]};" +
                    $"password={Configuration["ConnectionString:Password"]};" +
                    $"database={Configuration["ConnectionString:Database"]}");
            }
        }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
