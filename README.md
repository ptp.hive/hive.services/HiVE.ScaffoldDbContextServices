# HiVE.ScaffoldDbContextServices

***[HiVE.Scaffold-DbContextServices]***

Generates code for a _DbContext_ and entity types for a database. In order for ***Scaffold-DbContext*** to generate an entity type, the database table must have a _primary key_.

## Notice:
If you see a System.IO error or something like that, please make sure the **path is short**.

Also, some **characters** in the path may fail the command.

Char sample issues:
`'[', ']'`

> Could not load file or assembly 'System.Runtime, Version=4.2.2.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a'. The located assembly's manifest definition does not match the assembly reference. (Exception from HRESULT: 0x80131040)